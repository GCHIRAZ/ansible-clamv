#!/bin/bash


rm -r /tmp/dir-path/*
rm -r /tmp/dir/*

rm /tmp/global-report.txt
rm /tmp/global-path.txt

for ct in $CTall ; do pct exec "$ct" -- bash -c 'rm /tmp/path.txt' ; done
for ct in $CTall ; do pct exec "$ct" -- bash -c 'rm /var/log/clamav/resume-report.log' ; done
for ct in $CTall ; do pct exec "$ct" -- bash -c 'rm /tmp/resume-report.log' ; done

mkdir /tmp/dir
mkdir /tmp/dir-path

touch global-report.txt
touch global-path.txt


#dpkg -l | grep clam
#rm -r /var/lib/clamav/
#apt purge clamav-freshclam
#apt purge clamav-base
#apt install clamav
